/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from 'react';
import { useLocation } from 'react-router';
import { NavLink } from 'react-router-dom';
import SVG from 'react-inlinesvg';
import { toAbsoluteUrl, checkIsActive } from '../../../../_helpers';

export function AsideMenuList({ layoutProps }) {
	const location = useLocation();
	const getMenuItemActive = (url, hasSubmenu = false) => {
		return checkIsActive(location, url)
			? ` ${!hasSubmenu &&
					'menu-item-active'} menu-item-open menu-item-not-hightlighted`
			: '';
	};

	return (
		<>
			<ul className={`menu-nav ${layoutProps.ulClasses}`}>
				<li
					className={`menu-item menu-item-submenu ${getMenuItemActive(
						'/e-commerce',
						true
					)}`}
					aria-haspopup='true'
					data-menu-toggle='hover'>
					<NavLink className='menu-link menu-toggle' to='/e-commerce'>
						<span className='svg-icon menu-icon'>
							<SVG
								src={toAbsoluteUrl('/media/svg/icons/Text/Bullet-list.svg')}
							/>
						</span>
						<span className='menu-text'>Danh mục</span>
					</NavLink>
					<div className='menu-submenu'>
						<i className='menu-arrow' />
						<ul className='menu-subnav'>
							<li
								className={`menu-item ${getMenuItemActive(
									'/e-commerce/customers'
								)}`}
								aria-haspopup='true'>
								<NavLink className='menu-link' to='/e-commerce/customers'>
									<i className='menu-bullet menu-bullet-dot'>
										<span />
									</i>
									<span className='menu-text'>Danh sách công nợ</span>
								</NavLink>
							</li>
							<li
								className={`menu-item ${getMenuItemActive(
									'/e-commerce/products'
								)}`}
								aria-haspopup='true'>
								<NavLink className='menu-link' to='/e-commerce/products'>
									<i className='menu-bullet menu-bullet-dot'>
										<span />
									</i>
									<span className='menu-text'>Thống kê</span>
								</NavLink>
							</li>
						</ul>
					</div>
				</li>
			</ul>
		</>
	);
}
