export default [
	{
		id: 1,
		billId: 123,
		firstName: 'Sonni',
		lastName: 'Gabotti',
		email: 'sgabotti0@wsj.com',
		userName: 'sgabotti0',
		gender: 'Female',
		status: 1,
		dateOfBbirth: '10/14/1950',
		ipAddress: '251.237.126.210',
		type: 1,
		_userId: 1,
		_createdDate: '09/07/2016',
		_updatedDate: '05/31/2013',
	},
];
