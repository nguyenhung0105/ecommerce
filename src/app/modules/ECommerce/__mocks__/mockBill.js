import billTableMock from './billTableMock';
import MockUtils from './mock.utils';

export default function mockBill(mock) {
	mock.onPost('api/bills').reply(({ data }) => {
		const { customer } = JSON.parse(data);
		const {
			billId = '',
			firstName = '',
			lastName = '',
			amount = '',
			status = '',
			email = '',
			tradeDate = '01/01/2019',
			type = '',
		} = customer;

		const id = generateUserId();
		const newCustomer = {
			id,
			billId,
			firstName,
			lastName,
			amount,
			status,
			email,
			tradeDate,
			type,
		};
		billTableMock.push(newCustomer);
		return [200, { customer: newCustomer }];
	});

	mock.onPost('api/bills/find').reply(config => {
		const mockUtils = new MockUtils();
		const { queryParams } = JSON.parse(config.data);
		const filterdCustomers = mockUtils.baseFilter(billTableMock, queryParams);
		return [200, filterdCustomers];
	});

	mock.onPost('api/bills/deleteCustomers').reply(config => {
		const { ids } = JSON.parse(config.data);
		ids.forEach(id => {
			const index = billTableMock.findIndex(el => el.id === id);
			if (index > -1) {
				billTableMock.splice(index, 1);
			}
		});
		return [200];
	});

	mock.onPost('api/bills/updateStatusForCustomers').reply(config => {
		const { ids, status } = JSON.parse(config.data);
		billTableMock.forEach(el => {
			if (ids.findIndex(id => id === el.id) > -1) {
				el.status = status;
			}
		});
		return [200];
	});

	mock.onGet(/api\/bills\/\d+/).reply(config => {
		const id = config.url.match(/api\/bills\/(\d+)/)[1];
		const customer = billTableMock.find(el => el.id === +id);
		if (!customer) {
			return [400];
		}

		return [200, customer];
	});

	mock.onPut(/api\/bills\/\d+/).reply(config => {
		const id = config.url.match(/api\/bills\/(\d+)/)[1];
		const { customer } = JSON.parse(config.data);
		const index = billTableMock.findIndex(el => el.id === +id);
		if (!index) {
			return [400];
		}

		billTableMock[index] = { ...customer };
		return [200];
	});

	mock.onDelete(/api\/bills\/\d+/).reply(config => {
		const id = config.url.match(/api\/bills\/(\d+)/)[1];
		const index = billTableMock.findIndex(el => el.id === +id);
		billTableMock.splice(index, 1);
		if (!index === -1) {
			return [400];
		}

		return [200];
	});
}

function generateUserId() {
	const ids = billTableMock.map(el => el.id);
	const maxId = Math.max(...ids);
	return maxId + 1;
}
