export const CustomerStatusCssClasses = ['danger', 'success', ''];
export const CustomerStatusTitles = ['Đã thanh toán', 'Chưa thanh toán'];
export const CustomerTypeCssClasses = ['success', 'primary', ''];
export const CustomerTypeTitles = ['Cash', 'Card', ''];
export const defaultSorted = [{ dataField: 'id', order: 'asc' }];
export const sizePerPageList = [
	{ text: '3', value: 3 },
	{ text: '5', value: 5 },
	{ text: '10', value: 10 },
];
export const initialFilter = {
	filter: {
		lastName: '',
		firstName: '',
		email: '',
	},
	sortOrder: 'asc', // asc||desc
	sortField: 'id',
	pageNumber: 1,
	pageSize: 10,
};
