// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import { Field, Form, Formik } from 'formik';
import React from 'react';
import { Modal } from 'react-bootstrap';
import * as Yup from 'yup';
import { Input, Select } from '../../../../../../_metronic/_partials/controls';

// Validation schema
const CustomerEditSchema = Yup.object().shape({
	firstName: Yup.string()
		.min(3, 'tối thiểu 3 ký tự')
		.max(50, 'tối đa 50 ký tự')
		.required('chưa điền tên'),
	lastName: Yup.string()
		.min(3, 'tối thiểu 3 ký tự')
		.max(50, 'tối đa 50 ký tự')
		.required('Chưa điền họ'),
	billId: Yup.string()
		.min(3, 'tối thiểu 3 ký tự')
		.max(50, 'tối đa 50 ký tự')
		.required('Chưa điền mã hóa đơn'),
	email: Yup.string()
		.email('Email chưa đúng')
		.required('Chưa điền Email'),
	tradeDate: Yup.mixed()
		.nullable(false)
		.required('Chưa điền ngày xuất hóa đơn'),
});

export function CustomerEditForm({
	saveCustomer,
	customer,
	actionsLoading,
	onHide,
}) {
	return (
		<>
			<Formik
				enableReinitialize={true}
				initialValues={customer}
				validationSchema={CustomerEditSchema}
				onSubmit={values => {
					saveCustomer(values);
				}}>
				{({ handleSubmit }) => (
					<>
						<Modal.Body className='overlay overlay-block cursor-default'>
							{actionsLoading && (
								<div className='overlay-layer bg-transparent'>
									<div className='spinner spinner-lg spinner-success' />
								</div>
							)}
							<Form className='form form-label-right'>
								<div className='form-group row'>
									{/* First Name */}
									<div className='col-lg-4'>
										<Field
											name='firstName'
											component={Input}
											placeholder='Tên'
											label='Tên'
										/>
									</div>
									{/* Last Name */}
									<div className='col-lg-4'>
										<Field
											name='lastName'
											component={Input}
											placeholder='Họ'
											label='Họ'
										/>
									</div>
									{/* tradeDate */}
									<div className='col-lg-4'>
										<Field
											name='tradeDate'
											component={Input}
											placeholder='Ngày xuất hóa đơn'
											label='Ngày xuất hóa đơn'
										/>
									</div>
								</div>
								{/* Email */}
								<div className='form-group row'>
									<div className='col-lg-4'>
										<Field
											type='email'
											name='email'
											component={Input}
											placeholder='Email'
											label='Email'
										/>
									</div>

									{/* Type */}
									<div className='col-lg-4'>
										<Select name='type' label='Type'>
											<option value='0'>Cash</option>
											<option value='1'>Card</option>
										</Select>
									</div>
									{/* Type */}
									<div className='col-lg-4'>
										<Select name='status' label='Status'>
											<option value='0'>Đã thanh toán</option>
											<option value='1'>Chưa thanh toán</option>
										</Select>
									</div>
								</div>

								<div className='form-group row'>
									<div className='col-lg-4'>
										<Field
											type='billId'
											name='billId'
											component={Input}
											placeholder='billId'
											label='BillId'
										/>
									</div>
									{/* Date of birth */}
									<div className='col-lg-4'>
										<Field
											type='amount'
											name='amount'
											component={Input}
											placeholder='công nơ'
											label='Công nợ'
										/>
									</div>
								</div>
							</Form>
						</Modal.Body>
						<Modal.Footer>
							<button
								type='button'
								onClick={onHide}
								className='btn btn-light btn-elevate'>
								Cancel
							</button>
							<> </>
							<button
								type='submit'
								onClick={() => handleSubmit()}
								className='btn btn-primary btn-elevate'>
								Save
							</button>
						</Modal.Footer>
					</>
				)}
			</Formik>
		</>
	);
}
