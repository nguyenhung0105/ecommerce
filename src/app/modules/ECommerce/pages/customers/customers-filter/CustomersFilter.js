import React, { useMemo } from 'react';
import { Formik } from 'formik';
import { isEqual } from 'lodash';
import { useCustomersUIContext } from '../CustomersUIContext';

const prepareFilter = (queryParams, values) => {
	const { status, type, searchText } = values;
	const newQueryParams = { ...queryParams };
	const filter = {};
	// Filter by status
	filter.status = status !== '' ? +status : undefined;
	// Filter by type
	filter.type = type !== '' ? +type : undefined;
	// Filter by all fields
	filter.lastName = searchText;
	if (searchText) {
		filter.firstName = searchText;
		filter.email = searchText;
		filter.ipAddress = searchText;
	}
	newQueryParams.filter = filter;
	return newQueryParams;
};

export function CustomersFilter({ listLoading }) {
	// Customers UI Context
	const customersUIContext = useCustomersUIContext();
	const customersUIProps = useMemo(() => {
		return {
			queryParams: customersUIContext.queryParams,
			setQueryParams: customersUIContext.setQueryParams,
		};
	}, [customersUIContext]);

	// queryParams, setQueryParams,
	const applyFilter = values => {
		const newQueryParams = prepareFilter(customersUIProps.queryParams, values);
		if (!isEqual(newQueryParams, customersUIProps.queryParams)) {
			newQueryParams.pageNumber = 1;
			// update list by queryParams
			customersUIProps.setQueryParams(newQueryParams);
		}
	};

	return (
		<>
			<Formik
				initialValues={{
					status: '',
					type: '',
					searchText: '',
				}}
				onSubmit={values => {
					applyFilter(values);
				}}>
				{({
					values,
					handleSubmit,
					handleBlur,
					handleChange,
					setFieldValue,
				}) => (
					<form onSubmit={handleSubmit} className='form form-label-right'>
						<div className='form-group row'>
							<div className='col-lg-2'>
								<select
									className='form-control'
									name='status'
									placeholder='Filter by Status'
									// TODO: Change this code
									onChange={e => {
										setFieldValue('status', e.target.value);
										handleSubmit();
									}}
									onBlur={handleBlur}
									value={values.status}>
									<option value=''>Tất cả</option>
									<option value='0'>Đã thanh toán</option>
									<option value='1'>Chưa thanh toán</option>
								</select>
								<small className='form-text text-muted'>
									<b>Lọc</b> theo Trạng thái
								</small>
							</div>
							<div className='col-lg-2'>
								<select
									className='form-control'
									placeholder='Filter by Type'
									name='type'
									onBlur={handleBlur}
									onChange={e => {
										setFieldValue('type', e.target.value);
										handleSubmit();
									}}
									value={values.type}>
									<option value=''>Tất cả</option>
									<option value='0'>Cash</option>
									<option value='1'>Card</option>
								</select>
								<small className='form-text text-muted'>
									<b>Lọc</b> theo type
								</small>
							</div>
							<div className='col-lg-2'>
								<input
									type='text'
									className='form-control'
									name='searchText'
									placeholder='Search'
									onBlur={handleBlur}
									value={values.searchText}
									onChange={e => {
										setFieldValue('searchText', e.target.value);
										handleSubmit();
									}}
								/>
								<small className='form-text text-muted'>Tìm kiếm</small>
							</div>
						</div>
					</form>
				)}
			</Formik>
		</>
	);
}
